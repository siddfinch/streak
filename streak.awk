BEGIN {
    score[away]=0
    score[home]=0
    team[away]=""
    team[home]=""
    old_gid=""
}
/^gameday/ {
  year=$2; month=$3; day=$4; game=$5
  home=$6; away=$7
  if ( home != "cle" ) home=toupper(home)
  if ( away != "cle" ) away=toupper(away)
  inning=$8 ;
  home_runs=$10; away_runs=$9;

  gid=sprintf("%04d%02d%02d%02d", year, month, day, game)
  if ( gid != old_gid ) {
    # new game
    old_gid=gid
    score[away]=0
    score[home]=0
    team[away]=away
    team[home]=home
  }

  score[away]=score[away]+away_runs
  away_diff=score[away] - score[home]
  if ( away_diff > 0 ) away_win=team[away]
  if ( away_diff < 0 ) away_win=team[home]
  if ( away_diff == 0 ) away_win="tie"

  score[home]=score[home]+home_runs
  home_diff=score[home] - score[away]
  if ( home_diff > 0 ) home_win=team[home]
  if ( home_diff < 0 ) home_win=team[away]
  if ( home_diff == 0 ) home_win="tie"

  print gid "," $0 "," score[away] "," score[home] "," away_diff "," home_diff "," away_win "," home_win
}
