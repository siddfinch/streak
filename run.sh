#!/bin/sh

/bin/sh ./streak.sh | \
	tee streak.out | \
	awk -F, -f streak.awk | tee streak.csv 
txt=""
for a in $(cut -f16,17 -d, streak.csv)
do 
	txt="$txt,$a" 
done
echo $txt | tee streak-line.txt | (
  tr , '\n' | awk '/[a-zA-Z]+/ {
   count[$1]=count[$1]+1
  } END { 
    for (a in count) { 
      printf "%4s%4d%6.2f\n", a, count[a], (count[a]*100.0)/NR 
    } 
  }') | sort -k 2 -nr | tee streak-totals.txt
