#!/bin/sh
gameday_url="http://gd2.mlb.com/components/game"
type="mlb"
team="cle"

incr_date(){
    if [ $day -gt 31 ]
    then
       day=0
       month=$(( month + 1 ))
   fi
   if [ $month -gt 12 ]
   then
       month=1
       year=$(( year + 1 ))
   fi
   day=$(( day + 1 )) 
}

get_gid(){
    gid=`curl -s $gameday_url/$type/year_${year_txt}/month_${month_txt}/day_${day_txt}/ | sed  -n -e 's/.*a href="\(.*'"$team"'.*\)".*/\1/p'`
}

get_boxscore(){
    count=1
    for game in $gid
    do
      away=`echo $game| sed -n -e 's/.*_\(...\)mlb_\(...\)mlb_.*/\1/p'`
      home=`echo $game| sed -n -e 's/.*_\(...\)mlb_\(...\)mlb_.*/\2/p'`
      echo "home: $home, away: $away"
      echo $gameday_url/$type/year_${year_txt}/month_${month_txt}/day_${day_txt}/$game
      for line in `curl -s $gameday_url/$type/year_${year_txt}/month_${month_txt}/day_${day_txt}/$game/boxscore.xml | sed -n -e 's/.*inning_line_score.*away="\(.*\)".*home="\(.*\)".*inning="\(.*\)".*/\3,\1,\2/p'`
     do
       echo gameday,$year,$month,$day,$count,$home,$away,$line
    done
     count=$(( count + 1 ))
    done
}

year=2017
month=8
day=24
today=`date +'%Y%m%d'`

while [ 1 ] 
do
    year_txt=`printf "%4d" $year`
    month_txt=`printf "%02d" $month`
    day_txt=`printf "%02d" $day`
    get_gid
    if [ "x$gid" = "x" ]
    then
      echo "no game id for $year/$month/$day"
    else
      get_boxscore
    fi
    count=$(( count ))
    if [ $count -gt 22 ]
    then
      exit 0
    fi
    incr_date
    echo ${year_txt}${month_txt}${day_txt}
    echo $today
    if [ "${year_txt}${month_txt}${day_txt}" = "$today" ]
    then
      exit 0
    fi     
    sleep 2
done
